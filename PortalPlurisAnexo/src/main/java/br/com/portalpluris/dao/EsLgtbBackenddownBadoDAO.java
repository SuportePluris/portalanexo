package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class EsLgtbBackenddownBadoDAO {

	public void registraLog(String oldIp, String newIp) {
		
		Connection conAnexo = DAO.getConexao();

		try {	
			
			PreparedStatement pst;

			pst = conAnexo.prepareStatement(""
					+"  INSERT INTO ES_LGTB_BACKENDDOWN_BADO( "
					+"  	BADO_DS_IPDOWN, "
					+ "		BADO_DS_IPUP "
					+ "	) VALUES( "
					+ "		?, "
					+ "		? "
					+ "	) "
					+" ");

			pst.setString(1, oldIp);
			pst.setString(2, newIp);
			pst.execute();		

		} catch (Exception e) {
			System.out.println("[registraLog] - ERRO AO INSERIR REGISTROS");
		}
	}
	
	public void updateLog() throws SQLException {
		
		Connection conAnexo = DAO.getConexao();

		PreparedStatement pst;

		pst = conAnexo.prepareStatement(""
				+"  UPDATE "
				+"		ES_LGTB_BACKENDDOWN_BADO "
				+"  SET "
				+ "		BADO_DH_RESTART = GETDATE() "
				+ "	WHERE "
				+ "		BADO_DH_RESTART IS NULL "
				+" ");

		pst.execute();		

	}
}
