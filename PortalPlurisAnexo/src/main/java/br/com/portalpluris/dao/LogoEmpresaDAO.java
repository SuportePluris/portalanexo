package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import br.com.portalpluris.bean.LogoEmpresaBean;

public class LogoEmpresaDAO {
	
	/**
	 * Método responsavel por realizar a consulta do logo da empresa
	 * */
	public LogoEmpresaBean getLogoEmpresaByIdEmpresa(int idEmbaCdEmpresaBanco) {

		LogoEmpresaBean logoEmpresa = new LogoEmpresaBean();	
		Connection conAnexo = DAO.getConexao();

		try {	
			// REALIZAR CONSULTA PARA OBTER DADOS DO BANCO
			PreparedStatement pst;
			ResultSet ret = null;

			pst = conAnexo.prepareStatement(""
					+"  SELECT "
					+"  	EMBA_DS_IMAGEM "
					+"  FROM  "
					+"  	CS_CDTB_EMPRESABANCO_EMBA (NOLOCK)  "
					+"  WHERE  "
					+"  	ID_EMBA_CD_EMPRESABANCO = ? "
					+" ");

			pst.setInt(1, idEmbaCdEmpresaBanco);
			ret = pst.executeQuery();		
			
			if(ret.next()) {			
				logoEmpresa.setLogoBase64(ret.getString("EMBA_DS_IMAGEM"));		
			}

		} catch (Exception e) {
			System.out.println("[AnexoBean] - ERRO AO OBTER LOGO");
		}

		return logoEmpresa;
		
	}

}
