package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DAO {
	
	private static HikariDataSource dsCatApi;
	private static Connection con;
	private static HikariDataSource dsCatApiEmpresa;
	private static Connection conEmpresa;

	/**
	 * Método responsável por realizar a conexão com o banco de dados.
	 * @author Caio Fernandes
	 * @return con
	 * 
	 * */
	public static Connection getConexao() {

		
		try {
			if (dsCatApi == null || con.isClosed()) {
				ConfiguracaoDataSourcer confDataSoucer = new ConfiguracaoDataSourcer("datasource.properties");

				HikariConfig confCatapi = new HikariConfig();
				confCatapi.setJdbcUrl(confDataSoucer.getUrl());
				confCatapi.setUsername(confDataSoucer.getUserName());
				confCatapi.setPassword(confDataSoucer.getPassword());
				confCatapi.setDriverClassName(confDataSoucer.getDriveClasse());
				dsCatApi = new HikariDataSource(confCatapi);
				con = dsCatApi.getConnection();
			}

			return con;

		} catch (Throwable e) {

			System.out.println(e);

		}
		return con;
	}
	
	/**
	 * Método responsável por realizar a conexão com o banco de dados de acordo com a empresa passada via parametro.
	 * @author Caio Fernandes
	 * @return con
	 * 
	 * */
	public Connection getConexaoByEmpresa(int idEmpresa) {
		
		Connection conDev = getConexao();
		String usuario = null;
		String senha = null;
		String nomeBancoDados = null;
		String ipServidor = null;
		
		try {
			
			// REALIZAR CONSULTA PARA OBTER DADOS DO BANCO
			PreparedStatement pst;
			ResultSet ret = null;
			
			pst = conDev.prepareStatement(""
				+"  SELECT "
				+"  	EMBA_DS_BANCO, "
				+"		EMBA_DS_USUARIO, "
				+"  	EMBA_DS_SENHA, "
				+"  	EMBA_DS_SERVIDOR "
				+"  FROM  "
				+"  	CS_CDTB_EMPRESABANCO_EMBA (NOLOCK)  "
				+"  WHERE  "
				+"  	ID_EMBA_CD_EMPRESABANCO = ? "
				+" ");

			pst.setInt(1, idEmpresa);

			ret = pst.executeQuery();
			
			if(ret.next()) {
				usuario = ret.getString("EMBA_DS_USUARIO");
				senha = ret.getString("EMBA_DS_SENHA");
				nomeBancoDados = ret.getString("EMBA_DS_BANCO");
				ipServidor = ret.getString("EMBA_DS_SERVIDOR");
			}
			
			
			// REALIZAR A NOVA CONEXAO COM O BANCO			
			if (dsCatApiEmpresa == null || conEmpresa.isClosed()) {
				
				HikariConfig confCatapi = new HikariConfig();
				confCatapi.setJdbcUrl("jdbc:sqlserver://" + ipServidor + ":1433" + ";DatabaseName=" + nomeBancoDados);
				confCatapi.setUsername(usuario);
				confCatapi.setPassword(senha);
				confCatapi.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				dsCatApi = new HikariDataSource(confCatapi);
				conEmpresa = dsCatApi.getConnection();
				
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}

		return conEmpresa;
	}
	
}
