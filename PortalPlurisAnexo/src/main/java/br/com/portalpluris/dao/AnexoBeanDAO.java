package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import br.com.portalpluris.bean.AnexoBean;

public class AnexoBeanDAO {


	/**
	 * Método responsavel por realizar a consulta na tabela de anexos
	 * */
	public ArrayList<AnexoBean> getAnexosByManif(int idChamCdChamado, int maniNrSequencia, int idEmbaCdEmpresaBanco) {

		ArrayList<AnexoBean> anexos = new ArrayList<AnexoBean>();
		DAO dao = new DAO();		
		Connection conAnexo = dao.getConexaoByEmpresa(idEmbaCdEmpresaBanco);

		try {	
			// REALIZAR CONSULTA PARA OBTER DADOS DO BANCO
			PreparedStatement pst;
			ResultSet ret = null;

			pst = conAnexo.prepareStatement(""
					+"  SELECT "
					+"  	MAAR.ID_CHAM_CD_CHAMADO, "
					+"		MAAR.MANI_NR_SEQUENCIA, "
					+"  	MAAR.MAAR_DS_MANIFARQUIVO, "
					+"  	BAZE64, "
					+"  	FORMAT (MASQ.TABE_DH_REGISTRO, 'dd/MM/yyyy hh:MM') AS DH_ANEXO "
					+"  FROM  "
					+"  	CS_ASTB_MANIFARQUIVO_MAAR MAAR (NOLOCK)  "
					+"  		INNER JOIN CS_ASTB_MANIFARQUIVO_SQ MASQ  "
					+"  			ON MAAR.ID_MAAR_CD_MANIFARQUIVO = MASQ.ID_TABE_CD_TABELA  "
					+"  		CROSS APPLY (SELECT MAAR_DS_ARQUIVO AS '*' FOR XML PATH('')) T (BAZE64)  "
					+"  WHERE  "
					+"  	MANI_NR_SEQUENCIA = ? "
					+"  	AND ID_CHAM_CD_CHAMADO = ? "
					+" ");

			pst.setInt(1, maniNrSequencia);
			pst.setInt(2, idChamCdChamado);
			ret = pst.executeQuery();		
			
			while(ret.next()) {			
				AnexoBean anexo = new AnexoBean();
				anexo.setNomeAnexo(ret.getString("MAAR_DS_MANIFARQUIVO"));
				anexo.setAnexoBinario(ret.getString("BAZE64"));
				anexo.setDtAnexo(ret.getString("DH_ANEXO"));				
				anexos.add(anexo);		
			}

		} catch (Exception e) {
			System.out.println("[AnexoBean] - ERRO AO OBTER ANEXOS");
		}

		return anexos;

	}

}
