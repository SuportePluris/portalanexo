package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import br.com.portalpluris.bean.FuncionarioBean;
import br.com.portalpluris.bean.FuncionarioPermissaoBean;
import br.com.portalpluris.bean.LoginBean;
import br.com.portalpluris.bean.RetornoBean;

public class EsCdtbFuncionarioFuncDao {
	
	public FuncionarioBean validaAutenticacao(FuncionarioBean funcBean){
		
		DAO dao = new DAO();
		Connection conn = dao.getConexao();
		ResultSet ret = null;
		
		try {
			PreparedStatement pst;

			pst = conn.prepareStatement(""
				+"  SELECT "
				+"  	FUNC.ID_FUNC_CD_FUNCIONARIO, "
				+"		FUNC.FUNC_DS_NOME, "
				+"  	FUNC.FUNC_DS_LOGINNAME, "
				+"  	FUNC.FUNC_DS_PASSWORD, "
				+"  	FUNC.FUNC_IN_LOGADO, "
				+"  	FUNC.FUNC_IN_PRIMEIROACESSO, "
				+"		FUNC.FUNC_IN_ATIVO, "
				+"  	FUPE.FUPE_IN_ENVIOEMAIL, "
				+"  	FUPE.FUPE_IN_TEMPLATEEMAIL, "
				+"  	FUPE.FUPE_IN_LOGIN, "
				+"  	FUPE.FUPE_IN_UPLOADCORREIOS, "
				+"  	FUPE.FUPE_IN_VISUALIZACOLETA, "
				+"  	FUPE.FUPE_IN_IMPORTACOLETA, "
				+"  	FUPE.FUPE_IN_VISUALIZAPOSTAGEM, "
				+"  	FUPE.FUPE_IN_IMPORTAPOSTAGEM, "
				+"  	FUPE.FUPE_IN_VISUALIZARELAGUIA, "
				+"  	FUPE.FUPE_IN_IMPORTARELAGUIA, "
				+"  	FUPE.FUPE_IN_LAYOUTEMAIL, "
				+"  	FUPE.FUPE_IN_VISUALIZAVOUCHEROPERADOR, "
				+"  	FUPE.FUPE_IN_VISUALIZAVOUCHERNESTLE, "
				+"  	FUPE.FUPE_IN_IMPORTAVOUCHEROPERADOR, "
				+"  	FUPE.FUPE_IN_IMPORTAVOUCHERNESTLE, "
				+"  	FUPE.FUPE_IN_VISUALIZAANALISEDIVOPERADOR, "
				+"  	FUPE.FUPE_IN_VISUALIZAANALISEDIVNESTLE, "
				+"  	FUPE.FUPE_IN_IMPORTAANALISEDIVOPERADOR, "
				+"  	FUPE.FUPE_IN_IMPORTAANALISEDIVNESTLE, "
				+"  	FUPE.FUPE_IN_VISUALIZAPONTOSOPERADOR, "
				+"  	FUPE.FUPE_IN_VISUALIZAPONTOSNESTLE, "
				+"  	FUPE.FUPE_IN_IMPORTAPONTOSOPERADOR, "
				+"  	FUPE.FUPE_IN_IMPORTAPONTOSNESTLE, "
				+"  	FUPE.FUPE_IN_VISUALIZAMESAOPERADOR, "
				+"  	FUPE.FUPE_IN_VISUALIZAMESANESTLE, "
				+"  	FUPE.FUPE_IN_IMPORTAMESAOPERADOR, "
				+"  	FUPE.FUPE_IN_IMPORTAMESANESTLE "
				+"  FROM  "
				+"  	CS_CDTB_FUNCIONARIO_FUNC FUNC (NOLOCK)  "
				+"  	LEFT JOIN CS_ASTB_FUNCIONARIOPERMISSAO_FUPE FUPE (NOLOCK) "
				+"  		ON FUNC.ID_FUNC_CD_FUNCIONARIO = FUPE.ID_FUNC_CD_FUNCIONARIO "
				+"  WHERE  "
				+"  	FUNC.FUNC_DS_LOGINNAME = ? "
				+"		AND FUNC.FUNC_DS_PASSWORD = ? "
				+"		AND FUNC.FUNC_IN_ATIVO = ? "
				+"");

			pst.setString(1, funcBean.getUser());
			pst.setString(2, funcBean.getPassword());
			pst.setString(3, "S");

			ret = pst.executeQuery();
		
			if(ret.next()){
				
				funcBean = populaBean(ret);
			}
			
			conn.close();
			pst.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return funcBean;
	}

	
	public LoginBean getLogin(LoginBean loginBean){
		
		DAO dao = new DAO();
		Connection conn = dao.getConexaoByEmpresa(loginBean.getIdEmbaCdEmpresabanco());
		ResultSet ret = null;
		
		try {
			PreparedStatement pst;

			pst = conn.prepareStatement(""
				+"  SELECT "
				+"  	ID_FUNC_CD_FUNCIONARIO, "
				+"		FUNC_NM_FUNCIONARIO, "
				+"  	FUNC_DS_LOGINNAME, "
				+"  	FUNC_DS_PASSWORD "
				+"  FROM  "
			    +"  	CS_CDTB_FUNCIONARIO_FUNC (NOLOCK)  "
				+"  WHERE  "
				+"  	FUNC_DS_LOGINNAME = ? "
				+"		AND FUNC_DS_PASSWORD	 = ? "
				+" ");

			pst.setString(1, loginBean.getFuncDsLoginname());
			pst.setString(2, loginBean.getFuncDsPassword());

			ret = pst.executeQuery();
		
			loginBean = new LoginBean();
			
			if(ret.next()){
				
				loginBean = populaBeanLogin(ret);
			}
			
			conn.close();
			pst.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return loginBean;
	}
	
	public LoginBean selectUserByUserName(String username){
		
		DAO dao = new DAO();
		Connection conn = dao.getConexao();
		ResultSet ret = null;
		
		LoginBean loginBean = new LoginBean();
		
		try {
			PreparedStatement pst;

			pst = conn.prepareStatement(""
				+"  SELECT "
				+"  	FUNC.ID_FUNC_CD_FUNCIONARIO, "
				+"		FUNC.FUNC_NM_FUNCIONARIO, "
				+"  	FUNC.FUNC_DS_LOGINNAME, "
				+"  	FUNC.FUNC_DS_PASSWORD,"
				+"		FUNC.FUNC_DS_PERMISSAO "
				+"  FROM  "
				+"  	CS_CDTB_FUNCIONARIO_FUNC FUNC (NOLOCK)  "
				+"  WHERE  "
				+"  	FUNC.FUNC_DS_LOGINNAME = ? "
				+"		AND FUNC.FUNC_IN_ATIVO = ? "
				+" ");

			pst.setString(1, username);
			pst.setString(2, "S");

			ret = pst.executeQuery();
		
			if(ret.next()){
				
				loginBean = populaBeanLogin(ret);
			}
			
			conn.close();
			pst.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return loginBean;
	}
	
	private LoginBean populaBeanLogin(ResultSet ret) throws SQLException {
		
		LoginBean funcBean = new LoginBean();
		
		funcBean.setIdFuncCdFuncionario(ret.getLong("ID_FUNC_CD_FUNCIONARIO"));
		funcBean.setFuncNmFuncionario(ret.getString("FUNC_NM_FUNCIONARIO")); 
		funcBean.setFuncDsLoginname(ret.getString("FUNC_DS_LOGINNAME"));
		funcBean.setFuncDsPassword(ret.getString("FUNC_DS_PASSWORD"));
		
		ResultSetMetaData meta = ret.getMetaData();
		for (int x=1; x <= meta.getColumnCount(); x++){
			
			if (meta.getColumnName(x).equalsIgnoreCase("FUNC_DS_PERMISSAO")) {
				funcBean.setFuncDsPermissao(ret.getString("FUNC_DS_PERMISSAO"));
			}
			
		}
		
		funcBean.setLoginValidated(true);
		
		return funcBean;
	}
	
	private FuncionarioBean populaBean(ResultSet ret) throws SQLException {
		
		FuncionarioBean funcBean = new FuncionarioBean();
		FuncionarioPermissaoBean fupeBean = new FuncionarioPermissaoBean();
		
		funcBean.setId(ret.getLong("ID_FUNC_CD_FUNCIONARIO"));
		funcBean.setNome(ret.getString("FUNC_DS_NOME"));
		funcBean.setUser(ret.getString("FUNC_DS_LOGINNAME"));
		funcBean.setPassword(ret.getString("FUNC_DS_PASSWORD"));
		funcBean.setPrimeiroAcesso(ret.getString("FUNC_IN_PRIMEIROACESSO"));
		funcBean.setLogado(ret.getString("FUNC_IN_LOGADO"));
		funcBean.setAtivo(ret.getString("FUNC_IN_ATIVO"));
		
		fupeBean.setId(ret.getLong("ID_FUNC_CD_FUNCIONARIO"));
		fupeBean.setEnvioEmail(ret.getString("FUPE_IN_ENVIOEMAIL"));
		fupeBean.setImportaAnaliseDivNestle(ret.getString("FUPE_IN_VISUALIZAANALISEDIVNESTLE"));
		fupeBean.setImportaAnaliseDivOperador(ret.getString("FUPE_IN_IMPORTAANALISEDIVOPERADOR"));
		fupeBean.setImportaColeta(ret.getString("FUPE_IN_IMPORTACOLETA"));
		fupeBean.setImportaMesaNestle(ret.getString("FUPE_IN_VISUALIZAMESANESTLE"));
		fupeBean.setImportaMesaOperador(ret.getString("FUPE_IN_VISUALIZAMESAOPERADOR"));
		fupeBean.setImportaPontosNestle(ret.getString("FUPE_IN_VISUALIZAPONTOSNESTLE"));
		fupeBean.setImportaPontosOperador(ret.getString("FUPE_IN_VISUALIZAPONTOSOPERADOR"));
		fupeBean.setImportaPostagem(ret.getString("FUPE_IN_IMPORTAPOSTAGEM"));
		fupeBean.setImportaRelAguia(ret.getString("FUPE_IN_IMPORTARELAGUIA"));
		fupeBean.setImportaVoucherNestle(ret.getString("FUPE_IN_IMPORTAVOUCHERNESTLE"));
		fupeBean.setImportaVoucherOperador(ret.getString("FUPE_IN_IMPORTAVOUCHEROPERADOR"));
		fupeBean.setLayoutEmail(ret.getString("FUPE_IN_LAYOUTEMAIL"));
		fupeBean.setLogin(ret.getString("FUPE_IN_LOGIN"));
		fupeBean.setTemplateEmail(ret.getString("FUPE_IN_TEMPLATEEMAIL"));
		fupeBean.setUploadCorreios(ret.getString("FUPE_IN_UPLOADCORREIOS"));
		fupeBean.setVisualizaAnaliseDivNestle(ret.getString("FUPE_IN_IMPORTAANALISEDIVNESTLE"));
		fupeBean.setVisualizaAnaliseDivOperador(ret.getString("FUPE_IN_VISUALIZAANALISEDIVOPERADOR"));
		fupeBean.setVisualizaColeta(ret.getString("FUPE_IN_VISUALIZACOLETA"));
		fupeBean.setVisualizaMesaNestle(ret.getString("FUPE_IN_IMPORTAMESANESTLE"));
		fupeBean.setVisualizaMesaOperador(ret.getString("FUPE_IN_IMPORTAMESAOPERADOR"));
		fupeBean.setVisualizaPontosNestle(ret.getString("FUPE_IN_IMPORTAPONTOSNESTLE"));
		fupeBean.setVisualizaPontosOperador(ret.getString("FUPE_IN_IMPORTAPONTOSOPERADOR"));
		fupeBean.setVisualizaPostagem(ret.getString("FUPE_IN_VISUALIZAPOSTAGEM"));
		fupeBean.setVisualizaRelAguia(ret.getString("FUPE_IN_VISUALIZARELAGUIA"));
		fupeBean.setVisualizaVoucherNestle(ret.getString("FUPE_IN_VISUALIZAVOUCHERNESTLE"));
		fupeBean.setVisualizaVoucherOperador(ret.getString("FUPE_IN_VISUALIZAVOUCHEROPERADOR"));
		
		funcBean.setFupeBean(fupeBean);
		
		return funcBean;
	}

	public RetornoBean criaUsuario(FuncionarioBean funcBean) {
		
		RetornoBean retBean = new RetornoBean();
		
		funcBean = validaAutenticacao(funcBean);
		
		if(funcBean.getId() == 0){
			
			DAO dao = new DAO();
			Connection conn = dao.getConexao();
			
			try {
				PreparedStatement pst;

				pst = conn.prepareStatement(""
					+" INSERT INTO CS_CDTB_FUNCIONARIO_FUNC ("
					+" 		FUNC_DS_NOME, "
					+" 		FUNC_DS_LOGINNAME, "
					+" 		FUNC_DS_PASSWORD, "
					+" 		FUNC_IN_PRIMEIROACESSO, "
					+" 		FUNC_IN_LOGADO, "
					+" 		FUNC_IN_ATIVO, "
					+ "		ID_FUNC_RESP"
					+" )VALUES (?, ?, ?, ?, ?, ?, ?)"
					+ "");

				pst.setString(1, funcBean.getNome());
				pst.setString(2, funcBean.getUser());
				pst.setString(3, funcBean.getPassword());
				pst.setString(4, "S");
				pst.setString(5, "N");
				pst.setString(6, "S");
				pst.setLong(7, funcBean.getIdFuncResp());

				pst.execute();
				
				funcBean.setId(getIdByUser(funcBean.getUser()));
				funcBean.getFupeBean().setId(funcBean.getId());

				pst = conn.prepareStatement(""
					+" INSERT INTO CS_ASTB_FUNCIONARIOPERMISSAO_FUPE ("
					+"		ID_FUNC_CD_FUNCIONARIO, "
					+" 		FUPE_IN_ENVIOEMAIL, "
					+" 		FUPE_IN_TEMPLATEEMAIL, "
					+" 		FUPE_IN_LOGIN, "
					+" 		FUPE_IN_UPLOADCORREIOS, "
					+" 		FUPE_IN_VISUALIZACOLETA, "
					+" 		FUPE_IN_IMPORTACOLETA, "
					+" 		FUPE_IN_VISUALIZAPOSTAGEM, "
					+" 		FUPE_IN_IMPORTAPOSTAGEM, "
					+" 		FUPE_IN_VISUALIZARELAGUIA, "
					+" 		FUPE_IN_IMPORTARELAGUIA, "
					+" 		FUPE_IN_LAYOUTEMAIL, "
					+" 		FUPE_IN_VISUALIZAVOUCHEROPERADOR, "
					+" 		FUPE_IN_VISUALIZAVOUCHERNESTLE, "
					+" 		FUPE_IN_IMPORTAVOUCHEROPERADOR, "
					+" 		FUPE_IN_IMPORTAVOUCHERNESTLE, "
					+" 		FUPE_IN_VISUALIZAANALISEDIVOPERADOR, "
					+" 		FUPE_IN_VISUALIZAANALISEDIVNESTLE, "
					+" 		FUPE_IN_IMPORTAANALISEDIVOPERADOR, "
					+" 		FUPE_IN_IMPORTAANALISEDIVNESTLE, "
					+" 		FUPE_IN_VISUALIZAPONTOSOPERADOR, "
					+" 		FUPE_IN_VISUALIZAPONTOSNESTLE, "
					+" 		FUPE_IN_IMPORTAPONTOSOPERADOR, "
					+" 		FUPE_IN_IMPORTAPONTOSNESTLE, "
					+" 		FUPE_IN_VISUALIZAMESAOPERADOR, "
					+" 		FUPE_IN_VISUALIZAMESANESTLE, "
					+" 		FUPE_IN_IMPORTAMESAOPERADOR, "
					+" 		FUPE_IN_IMPORTAMESANESTLE, "
					+" 		ID_FUNC_RESP "
					+" )VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
					+ "");
				
				pst.setLong(1, funcBean.getFupeBean().getId());
				pst.setString(2, funcBean.getFupeBean().getEnvioEmail());
				pst.setString(3, funcBean.getFupeBean().getTemplateEmail());
				pst.setString(4, funcBean.getFupeBean().getLogin());
				pst.setString(5, funcBean.getFupeBean().getUploadCorreios());
				pst.setString(6, funcBean.getFupeBean().getVisualizaColeta());
				pst.setString(7, funcBean.getFupeBean().getImportaColeta());
				pst.setString(8, funcBean.getFupeBean().getVisualizaPostagem());
				pst.setString(9, funcBean.getFupeBean().getImportaPostagem());
				pst.setString(10, funcBean.getFupeBean().getVisualizaRelAguia());
				pst.setString(11, funcBean.getFupeBean().getImportaRelAguia());
				pst.setString(12, funcBean.getFupeBean().getLayoutEmail());
				pst.setString(13, funcBean.getFupeBean().getVisualizaVoucherOperador());
				pst.setString(14, funcBean.getFupeBean().getVisualizaVoucherNestle());
				pst.setString(15, funcBean.getFupeBean().getImportaVoucherOperador());
				pst.setString(16, funcBean.getFupeBean().getImportaVoucherNestle());
				pst.setString(17, funcBean.getFupeBean().getVisualizaAnaliseDivOperador());
				pst.setString(18, funcBean.getFupeBean().getVisualizaAnaliseDivNestle());
				pst.setString(19, funcBean.getFupeBean().getImportaAnaliseDivOperador());
				pst.setString(20, funcBean.getFupeBean().getImportaAnaliseDivNestle());
				pst.setString(21, funcBean.getFupeBean().getVisualizaPontosOperador());
				pst.setString(22, funcBean.getFupeBean().getVisualizaPontosNestle());
				pst.setString(23, funcBean.getFupeBean().getImportaPontosOperador());
				pst.setString(24, funcBean.getFupeBean().getImportaPontosNestle());
				pst.setString(25, funcBean.getFupeBean().getVisualizaMesaOperador());
				pst.setString(26, funcBean.getFupeBean().getVisualizaMesaNestle());
				pst.setString(27, funcBean.getFupeBean().getImportaMesaOperador());
				pst.setString(28, funcBean.getFupeBean().getImportaMesaNestle());
				pst.setLong(29, funcBean.getFupeBean().getIdFuncResp());
				
				pst.execute();
				
				conn.close();
				pst.close();
				
				retBean.setSucesso(true);
				
			}catch(Exception e){
				e.printStackTrace();
				retBean.setSucesso(false);
				retBean.setMsg(e.getMessage());
			}
			
		}else{
			retBean.setSucesso(false);
			retBean.setMsg("Usuário já cadastrado");
		}
		
		return retBean;
	}

	private long getIdByUser(String user) throws SQLException {
		
		DAO dao = new DAO();
		Connection conn = dao.getConexao();
		PreparedStatement pst;
		ResultSet ret = null;
		
		long id = 0;
		
		pst = conn.prepareStatement(""
				+" SELECT "
				+"		ID_FUNC_CD_FUNCIONARIO "
				+ " FROM "
				+ "		CS_CDTB_FUNCIONARIO_FUNC "
				+" 	WHERE "
				+" 		FUNC_DS_LOGINNAME = ? "
				+ "");
		
		pst.setString(1, user);
		ret = pst.executeQuery();
		
		if(ret.next()){
			
			id = ret.getLong("ID_FUNC_CD_FUNCIONARIO");
		}
		
		conn.close();
		pst.close();
		
		return id;
	}

	public void logoutUsuario(long id) throws SQLException {

		DAO dao = new DAO();
		Connection conn = dao.getConexao();
		PreparedStatement pst;
		
		pst = conn.prepareStatement(""
				+" UPDATE CS_CDTB_FUNCIONARIO_FUNC "
				+" SET "
				+"		FUNC_IN_LOGADO = ? "
				+" WHERE "
				+"		ID_FUNC_CD_FUNCIONARIO = ? "
				+ "");
		
		pst.setString(1, "N");
		pst.setLong(2, id);
		pst.execute();
		
		conn.close();
		pst.close();
	}

	public RetornoBean redefinirSenha(long id, String oldPassword, String newPassword) throws SQLException {

		DAO dao = new DAO();
		Connection conn = dao.getConexao();
		PreparedStatement pst;
		ResultSet ret = null;
		
		RetornoBean retBean = new RetornoBean(); 
		
		pst = conn.prepareStatement(""
				+" SELECT "
				+"		FUNC_DS_PASSWORD "
				+ " FROM "
				+ "		CS_CDTB_FUNCIONARIO_FUNC "
				+" 	WHERE "
				+" 		ID_FUNC_CD_FUNCIONARIO = ? "
				+ "");
		
		pst.setLong(1, id);
		
		ret = pst.executeQuery();
		
		if(ret.next()){
			
			if(oldPassword.equals(ret.getString("FUNC_DS_PASSWORD"))){
				
				pst = conn.prepareStatement(""
					+" UPDATE CS_CDTB_FUNCIONARIO_FUNC "
					+" SET "
					+"		FUNC_DS_PASSWORD = ?"
					+" WHERE"
					+"		ID_FUNC_CD_FUNCIONARIO = ?"
					+ "");
				
				pst.setString(1, newPassword);
				pst.setLong(2, id);
				pst.execute();
				
				retBean.setSucesso(true);
				
			}else{
				retBean.setSucesso(false);
				retBean.setMsg("A senha antiga não está correta");
			}
		}else{
			retBean.setSucesso(false);
			retBean.setMsg("Usuário não encontrado");
		}
		
		conn.close();
		pst.close();
		
		return retBean;
	}

	public void inativaUsuario(long id)  throws SQLException {

		DAO dao = new DAO();
		Connection conn = dao.getConexao();
		PreparedStatement pst;
		
		pst = conn.prepareStatement(""
				+" UPDATE CS_CDTB_FUNCIONARIO_FUNC "
				+" SET "
				+"		FUNC_IN_ATIVO = ?"
				+" WHERE"
				+"		ID_FUNC_CD_FUNCIONARIO = ?"
				+ "");
		
		pst.setString(1, "N");
		pst.setLong(2, id);
		pst.execute();
		
		conn.close();
		pst.close();
	}
}
