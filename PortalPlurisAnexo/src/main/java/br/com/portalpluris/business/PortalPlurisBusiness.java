package br.com.portalpluris.business;

import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.portalpluris.bean.AnexoBean;
import br.com.portalpluris.bean.FuncionarioBean;
import br.com.portalpluris.bean.LoginBean;
import br.com.portalpluris.bean.LogoEmpresaBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.dao.AnexoBeanDAO;
import br.com.portalpluris.dao.EsCdtbFuncionarioFuncDao;
import br.com.portalpluris.dao.EsLgtbBackenddownBadoDAO;
import br.com.portalpluris.dao.LogoEmpresaDAO;

public class PortalPlurisBusiness {

	EsCdtbFuncionarioFuncDao funcDao = new EsCdtbFuncionarioFuncDao();
	
	/**
	 * Metodo responsavel por obter os dados dos anexos de acordo com o chamado e manifestacao passados via parametro
	 * @author Caio Fernandes
	 * @param int idChamCdChamado, int maniNrSequencia, int idEmbaCdEmpresaBanco
	 * @return String jsonRetorno
	 * */	
	public String getAnexosByManif(int idChamCdChamado, int maniNrSequencia, int idEmbaCdEmpresaBanco)  {

		String jsonRetorno = "";
		AnexoBeanDAO anexoDao = new AnexoBeanDAO();
		ArrayList<AnexoBean> anexos = new ArrayList<AnexoBean>();

		// REALIZAR CONSULTA DOS ANEXOS 
		anexos = anexoDao.getAnexosByManif(idChamCdChamado, maniNrSequencia, idEmbaCdEmpresaBanco);

		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(anexos);

		} catch (Exception e) {
			System.out.println("[getAnexosByManif] - ERRO AO CONVERTER BEAN EM JSON");
		}

		return jsonRetorno;
	}
	
	/**
	 * Metodo responsavel por obter informacoes do login do CRM do usuario
	 * @author Caio Fernandes
	 * @param LoginBean loginBean
	 * @return String jsonRetorno
	 * */
	public String getLogin(LoginBean loginBean)  {

		String jsonRetorno = "";

		EsCdtbFuncionarioFuncDao func = new EsCdtbFuncionarioFuncDao();

		loginBean = func.getLogin(loginBean);

		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(loginBean);

		} catch (Exception e) {
			System.out.println("[getLogin] - ERRO AO CONVERTER BEAN EM JSON");
		}

		return jsonRetorno;
	}
	
	/**
	 * Metodo responsavel por obter base64 da imagem referente ao logo da empresa
	 * @author Caio Fernandes
	 * @param int idEmbaCdEmpresaBanco
	 * @return String jsonRetorno
	 * */
	public String getLogoEmpresaByIdEmpresa(int idEmbaCdEmpresaBanco) {
		
		String jsonRetorno = "";
		
		LogoEmpresaBean logoEmpresa = new LogoEmpresaBean();
		LogoEmpresaDAO logoEmpresaDao = new LogoEmpresaDAO();
		
		logoEmpresa = logoEmpresaDao.getLogoEmpresaByIdEmpresa(idEmbaCdEmpresaBanco);

		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(logoEmpresa);

		} catch (Exception e) {
			System.out.println("[getLogoEmpresaByIdEmpresa] - ERRO AO CONVERTER BEAN EM JSON");
		}

		return jsonRetorno;
		
	}
	
	String IV = "AAAAAAAAAAAAAAAA";
	String textopuro = "#pluris@ndg!#?fsdfds";
	String chaveencriptacao = "0123456789abcdef";

	public byte[] encrypt(String textopuro, String chaveencriptacao) throws Exception {
		Cipher encripta = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
		SecretKeySpec key = new SecretKeySpec(chaveencriptacao.getBytes("UTF-8"), "AES");
		encripta.init(Cipher.ENCRYPT_MODE, key,new IvParameterSpec(IV.getBytes("UTF-8")));
		return encripta.doFinal(textopuro.getBytes("UTF-8"));
	}

	public String decrypt(byte[] textoencriptado, String chaveencriptacao) throws Exception{
		Cipher decripta = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
		SecretKeySpec key = new SecretKeySpec(chaveencriptacao.getBytes("UTF-8"), "AES");
		decripta.init(Cipher.DECRYPT_MODE, key,new IvParameterSpec(IV.getBytes("UTF-8")));
		return new String(decripta.doFinal(textoencriptado),"UTF-8");
	}

	public String validaLogin(FuncionarioBean funcBean)  {

		String jsonRetorno = "";

		EsCdtbFuncionarioFuncDao func = new EsCdtbFuncionarioFuncDao();

		// 1. REALIZAR BUSCA NA TABELA DE USUÁRIOS.
		// 2. ARMAZENAR RETORNO NO BEAN DE USUÁRIOS.

		funcBean = func.validaAutenticacao(funcBean);

		// 3. TRANSFORMAR BEAN DE USUÁRIOS EM JSON
		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(funcBean);

		} catch (Exception e) {
			System.out.println("[validaLogin] - ERRO AO CONVERTER BEAN EM JSON");
		}

		return jsonRetorno;
	}

	public String criaLogin(FuncionarioBean funcBean) {

		String jsonRetorno = "";

		RetornoBean retBean = new RetornoBean();

		if(!funcBean.getUser().equals("") && !funcBean.getPassword().equals("") && !funcBean.getNome().equals("")){

			retBean = funcDao.criaUsuario(funcBean);

		}else{

			retBean.setSucesso(false);
			retBean.setMsg("Usuário e Senha inválidos");
		}

		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(retBean);

		} catch (Exception e) {
			System.out.println("[criaLogin] - ERRO AO CONVERTER BEAN EM JSON");
		}

		return jsonRetorno;
	}


	public String logoutUsuario(long id) {

		String jsonRetorno = "";

		EsCdtbFuncionarioFuncDao funcDao = new EsCdtbFuncionarioFuncDao();
		RetornoBean retBean = new RetornoBean();

		try {
			funcDao.logoutUsuario(id);
			retBean.setSucesso(true);
		} catch (Exception e) {
			e.printStackTrace();

			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());
		}

		try {

			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(retBean);

		} catch (Exception e) {
			System.out.println("[logoutUsuario] - ERRO AO CONVERTER BEAN EM JSON");
		}
		return jsonRetorno;
	}


	public String redefinirSenha(long id, String oldPassword, String newPassword) {

		String jsonRetorno = "";

		EsCdtbFuncionarioFuncDao funcDao = new EsCdtbFuncionarioFuncDao();
		RetornoBean retBean = new RetornoBean();

		try {
			retBean = funcDao.redefinirSenha(id, oldPassword, newPassword);

		} catch (Exception e) {
			e.printStackTrace();

			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());
		}

		try {

			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(retBean);

		} catch (Exception e) {
			System.out.println("[redefinirSenha] - ERRO AO CONVERTER BEAN EM JSON");
		}
		return jsonRetorno;
	}


	public String inativaUsuario(long id) {

		String jsonRetorno = "";

		EsCdtbFuncionarioFuncDao funcDao = new EsCdtbFuncionarioFuncDao();
		RetornoBean retBean = new RetornoBean();

		try {
			funcDao.inativaUsuario(id);
			retBean.setSucesso(true);
		} catch (Exception e) {
			e.printStackTrace();

			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());
		}

		try {

			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(retBean);

		} catch (Exception e) {
			System.out.println("[logoutUsuario] - ERRO AO CONVERTER BEAN EM JSON");
		}
		return jsonRetorno;
	}

	public void logIp(String oldIp, String newIp) {
		
		EsLgtbBackenddownBadoDAO badoDAO = new EsLgtbBackenddownBadoDAO();		
		badoDAO.registraLog(oldIp.replace("--", "//"), newIp.replace("--", "//"));
	}

	public String limpaLog() {

		String ret = "";
		EsLgtbBackenddownBadoDAO badoDAO = new EsLgtbBackenddownBadoDAO();
		
		try {
			
			badoDAO.updateLog();
			ret = "sucesso";
		} catch (Exception e) {

			StackTraceElement[] stackTrace = e.getStackTrace();
			for (StackTraceElement erro : stackTrace) {
				ret += erro;
			}
		}
		
		return ret;
	}

}
