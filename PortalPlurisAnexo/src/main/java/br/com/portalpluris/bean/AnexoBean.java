package br.com.portalpluris.bean;

public class AnexoBean {
	
	String nomeAnexo;
	String anexoBinario;
	String dtAnexo;
	
	public String getNomeAnexo() {
		return nomeAnexo;
	}
	public void setNomeAnexo(String nomeAnexo) {
		this.nomeAnexo = nomeAnexo;
	}
	public String getAnexoBinario() {
		return anexoBinario;
	}
	public void setAnexoBinario(String anexoBinario) {
		this.anexoBinario = anexoBinario;
	}
	public String getDtAnexo() {
		return dtAnexo;
	}
	public void setDtAnexo(String dtAnexo) {
		this.dtAnexo = dtAnexo;
	}

	
}
