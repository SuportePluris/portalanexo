package br.com.portalpluris.bean;

public class FuncionarioPermissaoBean {

	long id = 0;
	String envioEmail;
	String templateEmail;
	String login;
	String uploadCorreios;
	String visualizaColeta;
	String importaColeta;
	String visualizaPostagem;
	String importaPostagem;
	String visualizaRelAguia;
	String importaRelAguia;
	String layoutEmail;
	String visualizaVoucherOperador;
	String visualizaVoucherNestle;
	String importaVoucherOperador;
	String importaVoucherNestle;
	String visualizaAnaliseDivOperador;
	String visualizaAnaliseDivNestle;
	String importaAnaliseDivOperador;
	String importaAnaliseDivNestle;
	String visualizaPontosOperador;
	String visualizaPontosNestle;
	String importaPontosOperador;
	String importaPontosNestle;
	String visualizaMesaOperador;
	String visualizaMesaNestle;
	String importaMesaOperador;
	String importaMesaNestle;
	long idFuncResp;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEnvioEmail() {
		return envioEmail;
	}
	public void setEnvioEmail(String envioEmail) {
		this.envioEmail = envioEmail;
	}
	public String getTemplateEmail() {
		return templateEmail;
	}
	public void setTemplateEmail(String templateEmail) {
		this.templateEmail = templateEmail;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getUploadCorreios() {
		return uploadCorreios;
	}
	public void setUploadCorreios(String uploadCorreios) {
		this.uploadCorreios = uploadCorreios;
	}
	public String getVisualizaColeta() {
		return visualizaColeta;
	}
	public void setVisualizaColeta(String visualizaColeta) {
		this.visualizaColeta = visualizaColeta;
	}
	public String getImportaColeta() {
		return importaColeta;
	}
	public void setImportaColeta(String importaColeta) {
		this.importaColeta = importaColeta;
	}
	public String getVisualizaPostagem() {
		return visualizaPostagem;
	}
	public void setVisualizaPostagem(String visualizaPostagem) {
		this.visualizaPostagem = visualizaPostagem;
	}
	public String getImportaPostagem() {
		return importaPostagem;
	}
	public void setImportaPostagem(String importaPostagem) {
		this.importaPostagem = importaPostagem;
	}
	public String getVisualizaRelAguia() {
		return visualizaRelAguia;
	}
	public void setVisualizaRelAguia(String visualizaRelAguia) {
		this.visualizaRelAguia = visualizaRelAguia;
	}
	public String getImportaRelAguia() {
		return importaRelAguia;
	}
	public void setImportaRelAguia(String importaRelAguia) {
		this.importaRelAguia = importaRelAguia;
	}
	public String getLayoutEmail() {
		return layoutEmail;
	}
	public void setLayoutEmail(String layoutEmail) {
		this.layoutEmail = layoutEmail;
	}
	public String getVisualizaVoucherOperador() {
		return visualizaVoucherOperador;
	}
	public void setVisualizaVoucherOperador(String visualizaVoucherOperador) {
		this.visualizaVoucherOperador = visualizaVoucherOperador;
	}
	public String getVisualizaVoucherNestle() {
		return visualizaVoucherNestle;
	}
	public void setVisualizaVoucherNestle(String visualizaVoucherNestle) {
		this.visualizaVoucherNestle = visualizaVoucherNestle;
	}
	public String getImportaVoucherOperador() {
		return importaVoucherOperador;
	}
	public void setImportaVoucherOperador(String importaVoucherOperador) {
		this.importaVoucherOperador = importaVoucherOperador;
	}
	public String getImportaVoucherNestle() {
		return importaVoucherNestle;
	}
	public void setImportaVoucherNestle(String importaVoucherNestle) {
		this.importaVoucherNestle = importaVoucherNestle;
	}
	public String getVisualizaAnaliseDivOperador() {
		return visualizaAnaliseDivOperador;
	}
	public void setVisualizaAnaliseDivOperador(String visualizaAnaliseDivOperador) {
		this.visualizaAnaliseDivOperador = visualizaAnaliseDivOperador;
	}
	public String getVisualizaAnaliseDivNestle() {
		return visualizaAnaliseDivNestle;
	}
	public void setVisualizaAnaliseDivNestle(String visualizaAnaliseDivNestle) {
		this.visualizaAnaliseDivNestle = visualizaAnaliseDivNestle;
	}
	public String getImportaAnaliseDivOperador() {
		return importaAnaliseDivOperador;
	}
	public void setImportaAnaliseDivOperador(String importaAnaliseDivOperador) {
		this.importaAnaliseDivOperador = importaAnaliseDivOperador;
	}
	public String getImportaAnaliseDivNestle() {
		return importaAnaliseDivNestle;
	}
	public void setImportaAnaliseDivNestle(String importaAnaliseDivNestle) {
		this.importaAnaliseDivNestle = importaAnaliseDivNestle;
	}
	public String getVisualizaPontosOperador() {
		return visualizaPontosOperador;
	}
	public void setVisualizaPontosOperador(String visualizaPontosOperador) {
		this.visualizaPontosOperador = visualizaPontosOperador;
	}
	public String getVisualizaPontosNestle() {
		return visualizaPontosNestle;
	}
	public void setVisualizaPontosNestle(String visualizaPontosNestle) {
		this.visualizaPontosNestle = visualizaPontosNestle;
	}
	public String getImportaPontosOperador() {
		return importaPontosOperador;
	}
	public void setImportaPontosOperador(String importaPontosOperador) {
		this.importaPontosOperador = importaPontosOperador;
	}
	public String getImportaPontosNestle() {
		return importaPontosNestle;
	}
	public void setImportaPontosNestle(String importaPontosNestle) {
		this.importaPontosNestle = importaPontosNestle;
	}
	public String getVisualizaMesaOperador() {
		return visualizaMesaOperador;
	}
	public void setVisualizaMesaOperador(String visualizaMesaOperador) {
		this.visualizaMesaOperador = visualizaMesaOperador;
	}
	public String getVisualizaMesaNestle() {
		return visualizaMesaNestle;
	}
	public void setVisualizaMesaNestle(String visualizaMesaNestle) {
		this.visualizaMesaNestle = visualizaMesaNestle;
	}
	public String getImportaMesaOperador() {
		return importaMesaOperador;
	}
	public void setImportaMesaOperador(String importaMesaOperador) {
		this.importaMesaOperador = importaMesaOperador;
	}
	public String getImportaMesaNestle() {
		return importaMesaNestle;
	}
	public void setImportaMesaNestle(String importaMesaNestle) {
		this.importaMesaNestle = importaMesaNestle;
	}
	public long getIdFuncResp() {
		return idFuncResp;
	}
	public void setIdFuncResp(long idFuncResp) {
		this.idFuncResp = idFuncResp;
	}
	
	
}
