package br.com.portalpluris.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.portalpluris.bean.LoginBean;
import br.com.portalpluris.dao.EsCdtbFuncionarioFuncDao;

/**CLASSE DE CONSULTA USUÁRIOS VALIDOS PARA ACESSO AOS SERVIÇOS**/
//@Service	
public class UsuarioService implements UserDetailsService{

	@Override
	public UserDetails loadUserByUsername(String username){
		
		EsCdtbFuncionarioFuncDao funcDao = new EsCdtbFuncionarioFuncDao();
		LoginBean usuarioBean = funcDao.selectUserByUserName(username);
		
		System.out.println("trying load user with ... " + username);
		
		UserDetails u = User
				.builder()
				.username(usuarioBean.getFuncDsLoginname())
				.password(usuarioBean.getFuncDsPassword())
				.roles(usuarioBean.getFuncDsPermissao())
				.build();
		
		return u;
		
//		return User
//				.builder()
//				.username(usuarioBean.getFuncDsLoginname())
//				.password(usuarioBean.getFuncDsPassword())
//				.roles(usuarioBean.getFuncDsPermissao())
//				.build();
		
	}
	
}
