package br.com.portalpluris.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

	@Bean
	public FilterRegistrationBean<CorsFilter> corsFilterRegistrationBean(){
		List<String> all = Arrays.asList("*");
		
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.setAllowedOrigins(all);
		corsConfiguration.setAllowedHeaders(all);
		corsConfiguration.setAllowedMethods(all);
		corsConfiguration.setAllowCredentials(true);
		
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", corsConfiguration);
		
		CorsFilter corsFilter = new CorsFilter(source);
		FilterRegistrationBean<CorsFilter> filter = new FilterRegistrationBean<>(corsFilter);
		filter.setOrder(Ordered.HIGHEST_PRECEDENCE);
		
		return filter;
		
	}
	
	
	/**
	 * Alterado forma de registro do CORS para aceitar 
	 */
	@Override
	public void addCorsMappings(CorsRegistry registry) {
	    
		 registry.addMapping("/**");
		
		 /*
		 registry.addMapping("/*").allowedOrigins("*").allowedMethods("GET", "POST", "OPTIONS", "PUT")
	            .allowedHeaders("Content-Type", "X-Requested-With", "accept", "Origin", "Access-Control-Request-Method","Access-Control-Request-Headers")
	            .exposedHeaders("Access-Control-Allow-Origin", "Access-Control-Allow-Credentials")
	            .allowCredentials(true).maxAge(3600);
	     */

		 /*
		 registry.addMapping("/api/**")
         .allowedOrigins("http://domain2.com")
         .allowedMethods("PUT", "DELETE")
         .allowedHeaders("header1", "header2", "header3")
         .exposedHeaders("header1", "header2")
         .allowCredentials(false).maxAge(3600);
         */
		 
	}
		
		
}
