package br.com.dolcegusto.teste;

import java.sql.Connection;

import org.junit.Assert;
import org.junit.Test;

import br.com.portalpluris.dao.DAO;

public class TesteDao {

	@Test
	public void testeConnection() {
	
		DAO dao = new DAO();
		Connection con = dao.getConexao();
	
		Assert.assertNotNull(con);
	}
}
